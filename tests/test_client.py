import collections
import unittest
from unittest import mock
from unittest.mock import patch, MagicMock

import confluent_kafka
from confluent_kafka.cimpl import KafkaError, KafkaException

import antares_client


class TestClientHelpers(unittest.TestCase):
    @patch("antares_client.client._call")
    @patch("antares_client.client.os")
    def test_get_ssl_ca_location_raises_file_not_found_error_if_cant_find_certs(
        self, mock_os, mock_call
    ):
        mock_os.path.exists.return_value = False
        mock_call.return_value = (1, "stdout", "stderr")
        with self.assertRaisesRegex(FileNotFoundError, "locate SSL"):
            antares_client.client._locate_ssl_certs_file()

    def test_merge_dictionary_gives_precedence_to_later_args(self):
        dict_a = {"key_a": "old", "key_b": "old"}
        dict_b = {"key_a": "new"}
        merged = {"key_a": "new", "key_b": "old"}
        self.assertEqual(
            antares_client.client._merge_dictionaries(dict_a, dict_b), merged
        )

    def test_merge_dictionary_ignores_none(self):
        dict_a = {"key_a": "old", "key_b": "old"}
        dict_b = {"key_a": None}
        merged = {"key_a": "old", "key_b": "old"}
        self.assertEqual(
            antares_client.client._merge_dictionaries(dict_a, dict_b), merged
        )


class TestClient(unittest.TestCase):

    TEST_CONFIG = {
        "host": "192.168.0.1",
        "port": "8080",
        "api_key": "my_key",
        "api_secret": "my_secret",
    }

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    def test_parse_message_raises_kafka_exception(self, *_):
        message = MagicMock(spec=confluent_kafka.Message)
        message.error.return_value.code.return_value = KafkaError._PARTITION_EOF
        with self.assertRaisesRegex(
            KafkaException, "{}".format(KafkaError._PARTITION_EOF)
        ):
            antares_client.client._parse_message(message)
        message.error.return_value.code.return_value = KafkaError.UNKNOWN
        with self.assertRaisesRegex(KafkaException, "{}".format(KafkaError.UNKNOWN)):
            antares_client.client._parse_message(message)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message", autospec=True)
    def test_poll_catches_kafka_exception_and_raises_antares_exception_if_not_eof(
        self, mock_parse_message, *_
    ):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        mock_parse_message.side_effect = [KafkaException(KafkaError.UNKNOWN)]
        with self.assertRaises(antares_client.AntaresException):
            client.poll(timeout=1)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message", autospec=True)
    def test_poll_ignores_kafka_eof_errors(self, mock_parse_message, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        mock_message = MagicMock(spec=confluent_kafka.Message)
        mock_message.topic.return_value = "topic"
        client._consumer.poll.return_value = mock_message
        mock_parse_message.side_effect = [
            KafkaException(KafkaError._PARTITION_EOF),
            mock.DEFAULT,
        ]
        mock_parse_message.return_value = {"key": "value"}
        topic, alert = client.poll(timeout=1)
        self.assertEqual(topic, "topic")
        self.assertEqual(alert, {"key": "value"})
        self.assertEqual(mock_parse_message.call_count, 2)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message")
    def test_poll_respects_timeout(self, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        client.poll(timeout=1)
        _, kwargs = client._consumer.poll.call_args
        self.assertEqual(kwargs["timeout"], 1)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client.time.process_time", autospec=True)
    @patch("antares_client.client._parse_message")
    def test_poll_returns_none_if_timeout(
        self, mock_parse_message, mock_process_time, *_
    ):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        client._consumer.poll.return_value = None
        mock_process_time.side_effect = [0.0, 0.5, 1.5]
        topic, alert = client.poll(timeout=1)
        self.assertIsNone(topic)
        self.assertIsNone(alert)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message")
    def test_poll_returns_topic_and_alert(self, mock_parse_message, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        mock_message = MagicMock(spec=confluent_kafka.Message)
        mock_message.topic.return_value = "topic"
        client._consumer.poll.return_value = mock_message
        mock_parse_message.return_value = {"key": "value"}
        topic, alert = client.poll()
        self.assertEqual(topic, "topic")
        self.assertEqual(alert, {"key": "value"})

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message")
    def test_client_can_be_used_as_iterator(self, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        for _ in client.iter(num_alerts=10):
            pass
        self.assertEqual(client._consumer.poll.call_count, 10)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    @patch("antares_client.client._parse_message")
    def test_client_can_be_used_as_iterator(self, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        for topic, alert in client.iter(num_alerts=1):
            pass

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    def test_client_can_close_consumer(self, *_):
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        client.close()
        self.assertEqual(client._consumer.close.call_count, 1)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    def test_client_can_be_used_as_context_manager(self, *_):
        with antares_client.Client(["topic"], **self.TEST_CONFIG) as client:
            pass
        self.assertEqual(client._consumer.close.call_count, 1)

    @patch("antares_client.client._locate_ssl_certs_file", autospec=True)
    @patch("antares_client.client.confluent_kafka", autospec=True)
    def test_client_does_not_call_locate_ssl_certs_if_path_is_provided(
        self, _, mock_locate_ssl_certs_file
    ):
        """Issue #6"""
        config = self.TEST_CONFIG
        config["ssl_ca_location"] = "/path/to/cert.pem"
        client = antares_client.Client(["topic"], **self.TEST_CONFIG)
        self.assertFalse(mock_locate_ssl_certs_file.called)
