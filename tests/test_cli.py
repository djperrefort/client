import collections
import unittest
from unittest.mock import patch, MagicMock, mock_open

import antares_client.cli as cli


class TestCliArgumentParsing(unittest.TestCase):
    def test_parse_args_returns_dict_like_structure(self):
        args = cli.parse_args(["topic", "--api-key", "key", "--api-secret", "secret"])
        self.assertTrue(isinstance(args, collections.abc.Mapping))

    def test_parse_args_handles_multiple_topics(self):
        args = cli.parse_args(
            ["topic1,topic2", "--api-key", "key", "--api-secret", "secret"]
        )
        self.assertEqual(args["topics"], ["topic1", "topic2"])

    def test_parse_args_handles_single_topic(self):
        args = cli.parse_args(["topic", "--api-key", "key", "--api-secret", "secret"])
        self.assertEqual(args["topics"], ["topic"])


class TestCliMain(unittest.TestCase):
    def setUp(self):
        self.patchers = {
            "mock_json": patch("antares_client.cli.json"),
            "mock_log": patch("antares_client.cli.log"),
            "MockClient": patch("antares_client.cli.Client"),
            "mock_get_alert_id": patch(
                "antares_client.cli.get_alert_id", return_value="test_alert_id"
            ),
        }
        self.mock_alert = {"key": "val"}
        self.mock_get_alert_id = self.patchers["mock_get_alert_id"].start()
        self.mock_json = self.patchers["mock_json"].start()
        self.mock_log = self.patchers["mock_log"].start()
        self.MockClient = self.patchers["MockClient"].start()
        mock_client = MagicMock()
        mock_client.iter.return_value = [("test_topic", self.mock_alert)]
        self.MockClient.return_value.__enter__.return_value = mock_client

    def tearDown(self):
        for patcher in self.patchers.values():
            patcher.stop()

    @patch(
        "antares_client.cli.parse_args", return_value={"output_directory": "/some/dir"}
    )
    @patch("antares_client.cli.os.makedirs")
    def test_cli_writes_alert_to_file(self, *_):
        with patch.dict("antares_client.cli.__builtins__", {"open": mock_open()}) as _:
            cli.main()
        self.assertEqual(self.mock_json.dump.call_args[0][0], self.mock_alert)

    @patch("antares_client.cli.parse_args", return_value={})
    @patch("antares_client.cli.parse_args")
    def test_cli_logs_alert_if_no_output_directory(self, *_):
        cli.main()
        self.mock_log.info.assert_any_call("Received alert on topic 'test_topic'")
